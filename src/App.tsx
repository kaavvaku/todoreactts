import * as React from 'react';
import { useState } from 'react';
import './App.css';
import TodoList from './Components/TodoList';
import Context from './Context'
import AddTodo from './Components/AddTodo'


function App() {
  const [todos, setTodos] = useState([
    {id: 1, complited: false, title: "Изучить JavaScript"},
    {id: 2, complited: false, title: "Изучить Webpack"},
    {id: 3, complited: false, title: "Изучить Docker"},
    {id: 4, complited: false, title: "Изучить TypeScript"},
    {id: 5, complited: false, title: "Изучить React"},
    {id: 6, complited: false, title: "Изучить Redux"}
  ])

  function removeTodo(id:number) {
    setTodos(todos.map( (item:any,index:any) =>  item.id = index+1 ) )
    setTodos(todos.filter(item => item.id !== id))
  }

  function addTodo(title:string) {
    setTodos(
      todos.concat([
        { id: todos.length+1,
          complited: false, 
          title
        }])
    )
  }

  function onChange(id:number) {
    setTodos(
      todos.map((item,index) => {
        if(index+1 === id) {
          item.complited = !item.complited
        }
        return item
      })
    )
  }

  return (
    <Context.Provider value={{removeTodo, onChange}}>
      <div className="wrapper">
        <h1 className="h1">ToDo list</h1>
        <AddTodo onCreate={addTodo}></AddTodo>
        <TodoList todos={todos}></TodoList>
      </div>
    </Context.Provider>
  );
}

export default App;
