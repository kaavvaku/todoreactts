import * as React from 'react'
import TodoItem from './TodoItem'

function TodoList(props:any) {
    return (
       <ul className="ul">
           { props.todos.map( (item:any, index:number) => {
            return <TodoItem id={index+1} title={item.title} complited={item.complited} key={index} ></TodoItem>
           })}
       </ul>
    )
}

export default TodoList;