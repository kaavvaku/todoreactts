import React, { useContext } from 'react'
import Context from '../Context'

function TodoItem(todoItem:any) {
    const { removeTodo } = useContext(Context);
    const { onChange } = useContext(Context);
    const classes:string[] = [];

    if(todoItem.complited) {
        classes.push('done')
    }

    return (
        <li className="li">
            <span className={classes.join('')}>
                <input 
                    className="checkbox" 
                    type="checkbox"
                    onChange={()=> onChange(todoItem.id)}
                    checked={todoItem.complited}>
                </input>
                <span style={{marginRight: '1rem'}}>{todoItem.id + '.'}</span>
                {todoItem.title}
            </span>
            <button onClick={removeTodo.bind(null,todoItem.id)}>&times;</button>

        </li>
    )
}

export default TodoItem;