import React, { useState } from 'react'

function AddTodo({onCreate}:any) {
    const [value, setValue] = useState('')

    function submitHandler(event:any) {
        event.preventDefault()

        if(value.trim()) {
            onCreate(value)
            setValue('')
        }
    } 
    return (
        <form className="form" onSubmit={submitHandler}>
            <input className="input" placeholder="Изучить..." value={value} onChange={event => setValue(event.target.value)}></input>
            <button type="submit" className="button" >Добавить</button>
        </form>
    )
}

export default AddTodo